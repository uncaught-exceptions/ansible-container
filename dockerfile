FROM python:alpine
RUN apk add --no-cache \
      openssh-client \
      gcc \
      libffi-dev \
      openssl-dev \
      make \
      musl-dev \
      linux-headers; \
    pip install --upgrade pip && \
    pip install ansible;
